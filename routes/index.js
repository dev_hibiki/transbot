var http = require('request');
var cors = require('cors');
var uuid = require('uuid');
var mstranslator = require('mstranslator');

module.exports = function (app, addon) {
  var hipchat = require('../lib/hipchat')(addon);
  var trans = new mstranslator({
    client_id: "hipchat_xbot_trans",
    client_secret: "1id/itFPcYcQ7SVgYnEqSlAo1Z86L/NVKAPaVTsn4b0="
  }, true);

  // simple healthcheck
  app.get('/healthcheck', function (req, res) {
    res.send('OK');
  });

  // Root route. This route will serve the `addon.json` unless a homepage URL is
  // specified in `addon.json`.
  app.get('/',
    function(req, res) {
      // Use content-type negotiation to choose the best way to respond
      res.format({
        // If the request content-type is text-html, it will decide which to serve up
        'text/html': function () {
          res.redirect(addon.descriptor.links.homepage);
        },
        // This logic is here to make sure that the `addon.json` is always
        // served up when requested by the host
        'application/json': function () {
          res.redirect('/atlassian-connect.json');
        }
      });
    }
  );

  // https://www.hipchat.com/docs/apiv2/configurable
  app.get('/config',
    // Authenticates the request using the JWT token in the request
    addon.authenticate(),
    function(req, res) {
      // The `addon.authenticate()` middleware populates the following:
      // * req.clientInfo: useful information about the add-on client such as the
      //   clientKey, oauth info, and HipChat account info
      // * req.context: contains the context data accompanying the request like
      //   the roomId
      res.render('config', req.context);
    }
  );

  // https://www.hipchat.com/docs/apiv2/webhooks
  app.post('/webhook',
    addon.authenticate(),
    function(req, res) {
      var cmd = req.body.item.message.message;
      cmd = parseCommand(cmd);
      var params = {
        text: cmd.text,
        from: cmd.from || 'en',
        to: cmd.to
      };

      trans.translate(params, function(err, data) {
        var html = "";

        if (err) {
          html += addon.descriptor.name + " doesn't understand what you wanted to talk :(";
        } else {
          html += data;
        }

        hipchat.sendMessage(req.clientInfo, req.context.item.room.id, html)
          .then(function(data){
            res.send(200);
          });
      });
    }
  );

  /**
   * parse user input command
   * @param  {string} cmd
   * @return {object}
   *    {
   *      from: 'en',
   *      to: 'jp',
   *      text: 's t r i n g'
   *    }
   */
  function parseCommand (cmd) {
    var full = cmd.substr(cmd.indexOf(" ") + 1, cmd.length - 1);
    var command = full.substr(0, full.indexOf(" ") + 1),
        tokens = command.split(">");
    var from ,to;

    if (tokens.length === 1 ) {
      to = tokens[0];
    } else if (tokens.length >= 2) {
      from = tokens[0];
      to = tokens[1];
    }

    var text = full.substr(full.indexOf(command) + command.length);

    return {
      from: from,
      to: to,
      text: text
    };
  }

  // Notify the room that the add-on was installed
  addon.on('installed', function(clientKey, clientInfo, req){
    hipchat.sendMessage(clientInfo, req.body.roomId, 'The ' + addon.descriptor.name + ' add-on has been installed in this room');
  });

  // Clean up clients when uninstalled
  addon.on('uninstalled', function(id){
    addon.settings.client.keys(id+':*', function(err, rep){
      rep.forEach(function(k){
        addon.logger.info('Removing key:', k);
        addon.settings.client.del(k);
      });
    });
  });

};
