# Transbot

### whoami
A hubot helps you to translate between any of the supported languages.

### man
```
# call me to translate "text" from "from" language to "to" language
/trans from>to text
# /trans en>ja hello

# or to translate "text" from English (as default) to "to" language
/trans to text
# /trans ja hello

# about supported languages, check "Language Code" in below links
# https://msdn.microsoft.com/en-us/library/hh456380.aspx
```
